import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ui.MainScene;

public class UIRunner extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        MainScene mainScene = new MainScene();
        Scene scene = new Scene(mainScene);
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
