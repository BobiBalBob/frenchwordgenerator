package controller;

import java.util.Arrays;

public class CalculationUtils {

    public static Double[][] fromResultToPercent(Integer[][] results) {
        Double[][] percents = new Double[results.length][results[0].length];
        int current = 0;
        int next = 0;
        for(int currentRect = 0; currentRect < results.length; currentRect++) {
            double lineSum = Arrays.stream(results[current])
                    .map(nb -> nb == null ? 0 : nb)
                    .reduce(0, Integer::sum);
            for(int nextRect = 0; nextRect < results[0].length; nextRect++) {
                int nb = results[current][next] != null ?
                        results[current][next]
                        : 0;
                double percent = nb * 100 / lineSum;
                percents[current][next] = percent/100;
                next++;
            }
            next = 0;
            current++;
        }
        return percents;
    }
}
