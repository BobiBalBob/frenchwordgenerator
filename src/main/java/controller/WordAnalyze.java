package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

public class WordAnalyze {
    private static Integer[][] letterStatistics = new Integer[26][26];

    public static Integer[][] getLetterStatistics() {
        try {
            getWordFromFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return letterStatistics;
    }

    private static List<String> getWordFromFile() throws IOException {
        List<String> results = new ArrayList<String>();

        File file = new File(WordAnalyze.class.getClassLoader().getResource("mots.txt").getFile());
        BufferedReader br = new BufferedReader(new FileReader(file));

        String st;
        while ((st = br.readLine()) != null) {
            results.add(st);
            analyseWord(st);
        }
        return results;
    }

    private static int fromCharToArrayIndexes(char ch) {
        return ch - 'a';
    }

    private static void analyseWord(String word) {
        word = Normalizer.normalize(word, Normalizer.Form.NFD)
                .replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
        word = word.replace("-","");
        for (int i = 0; i < word.length() - 1; i++){
            char currentChar = word.toLowerCase().charAt(i);
            char nextChar = word.toLowerCase().charAt(i+1);
            int currentPosition = fromCharToArrayIndexes(currentChar);
            int nextPosition = fromCharToArrayIndexes(nextChar);
            if(currentPosition == 3 && nextPosition == 5 || currentPosition == 5 && nextPosition == 3 ) {
                System.out.println(word);
            }
            if(letterStatistics[currentPosition][nextPosition] == null) {
                letterStatistics[currentPosition][nextPosition] = 1;
            } else {
                letterStatistics[currentPosition][nextPosition] =
                        letterStatistics[currentPosition][nextPosition] + 1;
            }
        }

    }
}
