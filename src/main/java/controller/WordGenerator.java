package controller;

import java.util.HashMap;
import java.util.Map;

public class WordGenerator {
    private static Double equalProbability = 0.039d;

    public static String generateWord(Double[][] letterPercents, int size) {
        StringBuilder word = new StringBuilder();

        char lastLetter = generateLetter(generateEqualProbabilityArray());
        for(int i = 0; i < size; i++) {
            word.append(lastLetter);
            lastLetter = generateLetter(letterPercents[(int)lastLetter - 97]);
        }
        return word.toString();
    }

    public static char generateLetter(Double[] percents) {
        Map<Character, Double> mapCharPercent = new HashMap<>();
        for(int i = 0; i < percents.length; i++) {
            mapCharPercent.put(String.valueOf((char)(i + 97)).charAt(0),percents[i]);
        }
        double rng = Math.random();
        double currentSum = 0;
        for (Map.Entry<Character, Double> entry : mapCharPercent.entrySet()) {
            if(entry.getValue() > 0) {
                if(currentSum <= rng && rng <= currentSum + entry.getValue()) {
                    return entry.getKey();
                }
            }
            currentSum += entry.getValue();
        }
        return '#';
    }

    private static  Double[] generateEqualProbabilityArray() {
        Double[] percents = new Double[26];
        for(int i = 0; i < 26; i++) {
            percents[i] = equalProbability;
        }
        return percents;
    }
}
