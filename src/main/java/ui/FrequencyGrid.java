package ui;

import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class FrequencyGrid extends LetterGrid {
    private int WIDTH = 50;
    private int HEIGHT = 20;
    private final Color GREEN = Color.web("#4FE907");
    private final Color YELLOW = Color.web("#FFED0C");
    private final Color ORANGE = Color.web("#FF900C");
    private final Color RED = Color.web("#FF370C");
    private final Color DARK_RED = Color.web("#D92C07");

    public FrequencyGrid(Integer[][] statistics) {
        setGridLinesVisible(true);
        int current = 0;
        int next = 0;
        for(int currentRect = 0; currentRect < 27; currentRect++) {
            for(int nextRect = 0; nextRect < 27; nextRect++) {
                if(current > 0 && next > 0) {
                    Rectangle rectangle = new Rectangle(WIDTH, HEIGHT);
                    rectangle.setFill(getColorFromValue(statistics[current - 1][next - 1] != null ?
                            statistics[current - 1][next - 1]
                            : 0));
                    this.add(rectangle, next, current);
                } else {
                    Label label = new Label(gridLegend[current][next]);
                    label.setMinSize(WIDTH, HEIGHT);
                    this.add(label, next, current);
                }
                next++;
            }
            next = 0;
            current++;
        }
    }

    private Color getColorFromValue(Integer value) {
        if(value == 0) {
            return Color.BLACK;
        } else if(value < 10) {
            return GREEN;
        }else if(value < 1000) {
            return YELLOW;
        } else if(value < 10000) {
            return ORANGE;
        } else if(value < 100000) {
            return RED;
        }
        return DARK_RED;
    }
}
