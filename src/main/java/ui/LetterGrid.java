package ui;

import javafx.scene.layout.GridPane;

public class LetterGrid extends GridPane {
    protected String[][] gridLegend = new String[27][27];

    public LetterGrid() {
        gridLegend[0][1] = "A";
        gridLegend[0][2] = "B";
        gridLegend[0][3] = "C";
        gridLegend[0][4] = "D";
        gridLegend[0][5] = "E";
        gridLegend[0][6] ="F";
        gridLegend[0][7] ="G";
        gridLegend[0][8] ="H";
        gridLegend[0][9] ="I";
        gridLegend[0][10] ="J";
        gridLegend[0][11] ="K";
        gridLegend[0][12] ="L";
        gridLegend[0][13] ="M";
        gridLegend[0][14] ="N";
        gridLegend[0][15] ="O";
        gridLegend[0][16] ="P";
        gridLegend[0][17] ="Q";
        gridLegend[0][18] ="R";
        gridLegend[0][19] ="S";
        gridLegend[0][20] ="T";
        gridLegend[0][21] ="U";
        gridLegend[0][22] ="V";
        gridLegend[0][23] ="W";
        gridLegend[0][24] ="X";
        gridLegend[0][25] ="Y";
        gridLegend[0][26] ="Z";


        gridLegend[1][0] = "A";
        gridLegend[2][0] = "B";
        gridLegend[3][0] = "C";
        gridLegend[4][0] = "D";
        gridLegend[5][0] = "E";
        gridLegend[6][0] ="F";
        gridLegend[7][0] ="G";
        gridLegend[8][0] ="H";
        gridLegend[9][0] ="I";
        gridLegend[10][0] ="J";
        gridLegend[11][0] ="K";
        gridLegend[12][0] ="L";
        gridLegend[13][0] ="M";
        gridLegend[14][0] ="N";
        gridLegend[15][0] ="O";
        gridLegend[16][0] ="P";
        gridLegend[17][0] ="Q";
        gridLegend[18][0] ="R";
        gridLegend[19][0] ="S";
        gridLegend[20][0] ="T";
        gridLegend[21][0] ="U";
        gridLegend[22][0] ="V";
        gridLegend[23][0] ="W";
        gridLegend[24][0] ="X";
        gridLegend[25][0] ="Y";
        gridLegend[26][0] ="Z";
    }
}
