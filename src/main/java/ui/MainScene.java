package ui;

import controller.CalculationUtils;
import controller.WordAnalyze;
import controller.WordGenerator;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

import java.util.Optional;

public class MainScene extends GridPane {
    private StackPane tablePane;
    private PercentageGrid percentageGrid;
    private FrequencyGrid frequencyGrid;
    private Integer[][] statistics;

    public MainScene() {
        statistics = WordAnalyze.getLetterStatistics();
        tablePane = new StackPane();

        percentageGrid = new PercentageGrid(statistics);
        frequencyGrid = new FrequencyGrid(statistics);
        Button percentButton = new Button("Pourcentage");
        percentButton.setOnAction(actionEvent -> {
            tablePane.getChildren().clear();
            tablePane.getChildren().add(percentageGrid);
        });
        Button frequencyButton = new Button("Fréquence");
        frequencyButton.setOnAction(actionEvent -> {
            tablePane.getChildren().clear();
            tablePane.getChildren().add(frequencyGrid);
        });
        Button generateWordButton = new Button("Générer un nouveau mot");
        generateWordButton.setOnAction(actionEvent -> generateWord());
        tablePane.getChildren().add(percentageGrid);
        this.add(percentButton, 0, 0);
        this.add(frequencyButton, 1, 0);
        this.add(generateWordButton, 2, 0);
        this.add(tablePane, 0, 1, 3, 1);
    }

    private void generateWord(){
        String lettersNb = "ex: 4";
        TextInputDialog dialog = new TextInputDialog(lettersNb);
        dialog.setTitle("Combien de lettres ?");

        Optional<String> result = dialog.showAndWait();

        if (result.isPresent()){
            lettersNb = result.get();
        }

        StringBuilder message = new StringBuilder(WordGenerator.generateWord(CalculationUtils.fromResultToPercent(statistics),
                Integer.valueOf(lettersNb)));
        message.append(" ,");
        message.append(WordGenerator.generateWord(CalculationUtils.fromResultToPercent(statistics),
                Integer.valueOf(lettersNb)));
        message.append(" ,");
        message.append(WordGenerator.generateWord(CalculationUtils.fromResultToPercent(statistics),
                Integer.valueOf(lettersNb)));
        message.append(" ,");
        message.append(WordGenerator.generateWord(CalculationUtils.fromResultToPercent(statistics),
                Integer.valueOf(lettersNb)));
        message.append(" ,");
        message.append(WordGenerator.generateWord(CalculationUtils.fromResultToPercent(statistics),
                Integer.valueOf(lettersNb)));
        message.append("\n");
        message.append(WordGenerator.generateWord(CalculationUtils.fromResultToPercent(statistics),
                Integer.valueOf(lettersNb)));
        message.append(" ,");
        message.append(WordGenerator.generateWord(CalculationUtils.fromResultToPercent(statistics),
                Integer.valueOf(lettersNb)));
        message.append(" ,");
        message.append(WordGenerator.generateWord(CalculationUtils.fromResultToPercent(statistics),
                Integer.valueOf(lettersNb)));
        message.append(" ,");
        message.append(WordGenerator.generateWord(CalculationUtils.fromResultToPercent(statistics),
                Integer.valueOf(lettersNb)));

        TextArea textArea = new TextArea(message.toString());
        textArea.setEditable(false);
        textArea.setWrapText(true);
        GridPane gridPane = new GridPane();
        gridPane.setMaxWidth(Double.MAX_VALUE);
        gridPane.add(textArea, 0, 0);

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);

        alert.getDialogPane().setContent(gridPane);
        alert.showAndWait();

    }
}
