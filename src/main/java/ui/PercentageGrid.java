package ui;

import controller.CalculationUtils;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.util.Arrays;

public class PercentageGrid extends LetterGrid {
    private int WIDTH = 50;
    private int HEIGHT = 20;
    private final Color LIGHT_BLUE = Color.web("#07E9E6");
    private final Color BLUE = Color.web("#0790E9");
    private final Color DARK_BLUE = Color.web("#18439E");
    private final Color DARK_RED = Color.web("#D92C07");

    public PercentageGrid(Integer[][] statistics) {
        setGridLinesVisible(true);
        int current = 0;
        int next = 0;
        Double[][] percents = CalculationUtils.fromResultToPercent(statistics);
        for(int currentRect = 0; currentRect < 27; currentRect++) {
            for(int nextRect = 0; nextRect < 27; nextRect++) {
                if(current > 0 && next > 0) {
                    Rectangle rectangle = new Rectangle(WIDTH, HEIGHT);
                    rectangle.setFill(getColorFromPercent(percents[current - 1][next - 1]));
                    this.add(rectangle, next, current);
                    Label label = new Label(String.valueOf(Math.floor(percents[current - 1][next - 1] * 1000)/1000));
                    label.setMinSize(WIDTH, HEIGHT);
                    this.add(label, next, current);

                } else {
                    Label label = new Label(gridLegend[current][next]);
                    label.setMinSize(WIDTH, HEIGHT);
                    this.add(label, next, current);
                }
                next++;
            }
            next = 0;
            current++;
        }
    }

    private Color getColorFromPercent(double percent) {
        if(percent == 0) {
            return Color.BLACK;
        } else if(percent < 10) {
            return LIGHT_BLUE;
        } else if(percent < 50) {
            return BLUE;
        } else if(percent < 90) {
            return DARK_BLUE;
        }
        return DARK_RED;
    }
}
